function values(obj) {
    if (typeof (obj) != 'object' || obj == undefined) {
        return [];
    }
    else {
        let resValues = [];
        for(let i in obj){
            resValues.push(obj[i]);
        }
        return resValues;
    }
}

module.exports = values;