function keys(obj) {
    if (typeof (obj) != 'object' || obj == undefined) {
        return [];
    }
    else {
        let reskeys = [];
        for (let i in obj) {
            reskeys.push(i);
        }
        return reskeys;
    }
}

module.exports = keys