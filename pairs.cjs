function pairs(obj) {
    if (typeof (obj) != 'object' || obj == undefined) {
        return [];
    }
    else {
        let resPairs = [];
        for(i in obj){
            let keyValPair =[];
            keyValPair.push(i);
            keyValPair.push(obj[i]);
            resPairs.push(keyValPair);
        }
        return resPairs;
    }
}

module.exports = pairs;