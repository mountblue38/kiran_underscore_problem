function mapObject(obj,cbFunction) {
    if (typeof (obj) != 'object' || obj == undefined || typeof(cbFunction) != 'function' || cbFunction ==undefined ) {
        return [];
    }
    else {
        for(i in obj){
            x = cbFunction(obj[i],i);
            obj[i] = x;
        }
        return obj;
    }
}

module.exports = mapObject;
