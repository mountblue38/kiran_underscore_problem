function invert(obj) {
    if (typeof (obj) != 'object' || obj == undefined) {
        return [];
    }
    else {
        let resObj = {};
        for(i in obj){
            resObj[obj[i]] = String(i);
        }
        return resObj
    }
}

module.exports = invert;