function defaults(obj, def) {
    if (typeof (obj) != 'object' || obj == undefined || typeof (def) != 'object' || def == undefined) {
        return [];
    }
    else {
        for (let i in def) {
            if (obj[i] == undefined) {
                obj[i] = def[i];
            }
        }
        return obj;
    }
}

module.exports = defaults